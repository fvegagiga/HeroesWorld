//
//  SuperHeroes.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import Foundation


struct SuperHeroes: Codable {
    
    let superheroes: [Hero]
}


struct Hero: Codable {
    
    let name: String
    let photo: URL?
    let realName: String
    let height: String
    let power: String
    let abilities: String
    let groups: String
}
