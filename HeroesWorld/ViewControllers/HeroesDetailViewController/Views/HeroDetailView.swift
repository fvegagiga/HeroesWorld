//
//  HeroDetailView.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 14/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import UIKit

final class HeroDetailView: UIView {

    // MARK: - Properties
    
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var realNameLabel: UILabel!
    @IBOutlet private weak var heightLabel: UILabel!
    @IBOutlet private weak var powerLabel: UILabel!
    @IBOutlet private weak var abilitiesLabel: UILabel!
    @IBOutlet private weak var groupsLabel: UILabel!
    @IBOutlet private weak var mainStackView: UIStackView!

    // MARK: -
    
    var heroViewModel: HeroViewModel? {
        didSet {
            if let heroViewModel = heroViewModel {
                nameLabel.text = heroViewModel.name
                realNameLabel.text = heroViewModel.realName
                
                heightLabel.attributedText = attributedText(withString:
                    String(format: "\(TextLocalizedString.heightLabel): %@", heroViewModel.height),
                    boldString: "\(TextLocalizedString.heightLabel):",
                    font: heightLabel.font)
                
                powerLabel.attributedText = attributedText(withString:
                    String(format: "\(TextLocalizedString.powerLabel):\n\t%@", heroViewModel.power),
                    boldString: "\(TextLocalizedString.powerLabel):",
                    font: powerLabel.font)

                abilitiesLabel.attributedText = attributedText(withString:
                    String(format: "\(TextLocalizedString.abilitiesLabel):\n\t%@", heroViewModel.abilities),
                    boldString: "\(TextLocalizedString.abilitiesLabel):",
                    font: abilitiesLabel.font)

                groupsLabel.attributedText = attributedText(withString:
                    String(format: "\(TextLocalizedString.groupsLabel):\n\t%@", heroViewModel.groups),
                    boldString: "\(TextLocalizedString.groupsLabel):",
                    font: groupsLabel.font)
            }

            updateView()
        }
    }
    
    var photoImage: UIImage? {
        didSet {
            photoImageView.image = photoImage
        }
    }
    
    var orientationController: UIDeviceOrientation? {
        didSet {
            setStackViewOrientation()
        }
    }

    // MARK: - View Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configViews()
    }
    
    // MARK: - View Methods
    
    func updateView() {
        orientationController = UIDevice.current.orientation

        self.layoutIfNeeded()
    }
    
    func setStackViewOrientation() {
        if orientationController == .landscapeLeft || orientationController == .landscapeRight {
            mainStackView.axis = .horizontal
            mainStackView.alignment = .top
        } else {
            mainStackView.axis = .vertical
            mainStackView.alignment = .center
        }
    }
    
    func configViews() {
        
        // ImageView
        photoImageView.backgroundColor = Constants.ConfigData.ColorScheme.clearColor
        photoImageView.clipsToBounds = true
        photoImageView.contentMode = .scaleAspectFit
        let imageLayer = photoImageView.layer
        imageLayer.cornerRadius = 10.0
        imageLayer.borderWidth = 1.0
        imageLayer.borderColor = Constants.ConfigData.ColorScheme.mediumBackgroundColor.cgColor
        
        // Name
        if let font = UIFont(name: Constants.CustomFonts.boldCustomFontName, size: Constants.textSize.extrLargeTextSize)  {
            nameLabel.font = UIFontMetrics(forTextStyle: .title1).scaledFont(for: font)
        }
        nameLabel.numberOfLines = 2
        nameLabel.textColor = Constants.ConfigData.ColorScheme.customTextColor
        
        // realName
        if let font = UIFont(name: Constants.CustomFonts.standarCustomFontName, size: Constants.textSize.mediumTextSize)  {
            realNameLabel.font = UIFontMetrics(forTextStyle: .caption1).scaledFont(for: font)
        }
        realNameLabel.numberOfLines = 2
        realNameLabel.textColor = Constants.ConfigData.ColorScheme.customTextColor
        
        // heightLabel
        if let font = UIFont(name: Constants.CustomFonts.standarCustomFontName, size: Constants.textSize.largeTextSize)  {
            heightLabel.font = UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
        }
        heightLabel.numberOfLines = 2
        heightLabel.textColor = Constants.ConfigData.ColorScheme.customTextColor
        
        // powerLabel
        if let font = UIFont(name: Constants.CustomFonts.standarCustomFontName, size: Constants.textSize.largeTextSize)  {
            powerLabel.font = UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
        }
        powerLabel.numberOfLines = 0
        powerLabel.textColor = Constants.ConfigData.ColorScheme.customTextColor
        
        // powerLabel
        if let font = UIFont(name: Constants.CustomFonts.standarCustomFontName, size: Constants.textSize.largeTextSize)  {
            abilitiesLabel.font = UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
        }
        abilitiesLabel.numberOfLines = 0
        abilitiesLabel.textColor = Constants.ConfigData.ColorScheme.customTextColor
        
        // groupsLabel
        if let font = UIFont(name: Constants.CustomFonts.standarCustomFontName, size: Constants.textSize.largeTextSize)  {
            groupsLabel.font = UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
        }
        groupsLabel.numberOfLines = 0
        groupsLabel.textColor = Constants.ConfigData.ColorScheme.customTextColor
    }
    
}



// MARK: - Extension: attributedText

extension HeroDetailView {
    
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSAttributedStringKey.font: font])
        let newFont = UIFont.boldSystemFont(ofSize: Constants.textSize.mediumTextSize)
        let formatedFont = UIFontMetrics(forTextStyle: .title3).scaledFont(for: newFont)
        let boldFontAttribute: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: formatedFont]
        
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        
        return attributedString
    }
}
