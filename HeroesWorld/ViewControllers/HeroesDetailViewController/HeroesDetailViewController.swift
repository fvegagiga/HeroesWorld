//
//  HeroesDetailViewController.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import UIKit

final class HeroesDetailViewController: UIViewController {

    // MARK: - Properties
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heroDetailView: HeroDetailView!
    
    // MARK: -
    
    var heroViewModel: HeroViewModel?
    var imageHero: UIImage?
    
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        guard let heroViewModel = heroViewModel else { return }
        heroDetailView.heroViewModel = heroViewModel
        
        if let imageHero = imageHero {
            heroDetailView.photoImage = imageHero
        }
    }
    
    // MARK: - View Methods
    
    func setupView() {
        self.view.backgroundColor = Constants.ConfigData.ColorScheme.darkBackgroundColor
        heroDetailView.backgroundColor = Constants.ConfigData.ColorScheme.clearColor
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        heroDetailView.orientationController = UIDevice.current.orientation
    }

}
