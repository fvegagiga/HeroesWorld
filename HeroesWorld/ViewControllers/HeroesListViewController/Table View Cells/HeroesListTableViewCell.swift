//
//  HeroesListTableViewCell.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import UIKit

final class HeroesListTableViewCell: UITableViewCell {

    // MARK: - Constants
    
    static let reuseIdentifier = "HeroCell"
    
    // MARK: - Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var photoActivityIndicator: UIActivityIndicatorView!
    
    
    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Configure Cell appearance
        appearanceConfiguration()
    }
    
    // MARK: - Configuration
    
    func configure(imageCacheManager: ImageCacheManager?, withViewModel viewModel: HeroViewModel) {
        
        photoActivityIndicator.startAnimating()
        photoActivityIndicator.hidesWhenStopped = true
        
        nameLabel.text = viewModel.name
        photoImageView.image = UIImage(named:"anonymousHero")
        
        guard let cacheInUse = imageCacheManager, let urlPhoto = viewModel.photo else {
            photoActivityIndicator.stopAnimating()
            return
        }
        
        if let image = cacheInUse.cachedImage(url: urlPhoto) {
            // Image cached, set immediately
            photoImageView.image = image
            photoActivityIndicator.stopAnimating()
        } else {
            // Not cached, so load and then fade it in.
            cacheInUse.fetchImage(url: urlPhoto, completion: { [weak self] (image) in
                DispatchQueue.main.sync {
                    if let image = image {
                        self?.photoImageView.image = image.resizeImage(newWidth: 250)
                    }
                    self?.photoActivityIndicator.stopAnimating()
                }
            })
        }
    }
    
    
    func appearanceConfiguration() {
        
        self.selectionStyle = .none
        self.backgroundColor = Constants.ConfigData.ColorScheme.darkBackgroundColor
        
        // Name Label
        if let font = UIFont(name: Constants.CustomFonts.standarCustomFontName, size: Constants.textSize.largeTextSize) {
            nameLabel.font = UIFontMetrics(forTextStyle: .headline).scaledFont(for: font)
        }
        nameLabel.adjustsFontForContentSizeCategory = true
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.numberOfLines = 1
        nameLabel.textColor = Constants.ConfigData.ColorScheme.customTextColor
        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.allowsDefaultTighteningForTruncation = true
        nameLabel.lineBreakMode = .byTruncatingTail
        
        // ImageView
        photoImageView.backgroundColor = Constants.ConfigData.ColorScheme.clearColor
        photoImageView.clipsToBounds = true
        photoImageView.contentMode = .scaleAspectFill
    }
}
