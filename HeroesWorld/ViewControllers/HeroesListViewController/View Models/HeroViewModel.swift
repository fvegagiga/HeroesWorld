//
//  HeroViewModel.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import Foundation


struct HeroViewModel {
    
    let heroData: Hero

    var name: String {
        return heroData.name
    }
    
    var photo: URL? {
        if let photoHero = heroData.photo {
            return photoHero
        } else {
            return nil
        }
    }
    
    var realName: String {
        return heroData.realName
    }
    
    var height: String {
        return heroData.height
    }
    
    var power: String {
        return heroData.power
    }
    
    var abilities: String {
        return heroData.abilities
    }
    
    var groups: String {
        return heroData.groups
    }
}
