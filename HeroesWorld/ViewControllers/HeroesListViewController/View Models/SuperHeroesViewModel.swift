//
//  SuperHeroesViewModel.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import Foundation


struct SuperHeroesViewModel {
    
    // MARK: - Properties
    
    let superHeroes: [Hero]
    
    // MARK: -
    
    var numberOfSections: Int {
        return 1
    }
    
    var numberOfHeroes: Int {
        return superHeroes.count
    }
    
    // MARK: - Methods
    
    func viewModel(for index: Int) -> HeroViewModel {
        return HeroViewModel(heroData: superHeroes[index])
    }
    
}
