//
//  HeroesListViewController.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import UIKit

final class HeroesListViewController: UIViewController {

    // MARK: - Constants
    
    private let segueDetailView = "segueDetailView"
    
    // MARK: - Properties
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: -
    
    private lazy var dataManager: DataManager? = {
        guard let url = API.BaseURL else { return nil }
        return DataManager(baseURL: url)
    }()
    
    private lazy var imageCacheManager = {
        return ImageCacheManager()
    }()
    
    var heroesViewModel: SuperHeroesViewModel? {
        didSet {
            updateView()
        }
    }
    


    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        fetchHeroesData()
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        guard let sender = sender as? HeroesListTableViewCell else { return }
        
        switch identifier {
        case segueDetailView:
            guard let detailVC = segue.destination as? HeroesDetailViewController else { return }
            guard let indexPath = self.tableView.indexPathForSelectedRow else { return }
            
            // Configure Destination
            if let viewModel = heroesViewModel?.viewModel(for: indexPath.row) {
                detailVC.heroViewModel = viewModel
            }
            
            detailVC.imageHero = sender.photoImageView.image
            detailVC.navigationItem.largeTitleDisplayMode = .never
            
        default: break
        }
    }
    
    // MARK: - View Methods
    
    private func setupView() {
        title = Constants.ConfigData.AppTitle
        navigationController?.navigationBar.prefersLargeTitles = true
        
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.backgroundColor = Constants.ConfigData.ColorScheme.darkBackgroundColor
    }

    private func updateView() {
        if let _ = heroesViewModel {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    private func fetchHeroesData() {
        guard let dataManager = dataManager else { return }

        dataManager.requestHeroesData { [weak self] (response, error) in
            if let error = error {
                print(error.localizedDescription)
            } else if let response = response {
                self?.heroesViewModel = SuperHeroesViewModel(superHeroes: response)
            }
        }
    }
}


// MARK: - UITableViewDataSource

extension HeroesListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let heroesViewModel = heroesViewModel else { return 0 }
        return heroesViewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let heroesViewModel = heroesViewModel else { return 0 }
        return heroesViewModel.numberOfHeroes
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HeroesListTableViewCell.reuseIdentifier, for: indexPath) as? HeroesListTableViewCell else { fatalError("Unexpected Table View Cell") }
        
        if let viewModel = heroesViewModel?.viewModel(for: indexPath.row) {
            cell.configure(imageCacheManager: imageCacheManager, withViewModel: viewModel)
        }
        
        return cell
    }
}
