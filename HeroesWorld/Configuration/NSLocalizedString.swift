//
//  NSLocalizedString.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 15/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import Foundation

struct TextLocalizedString {
    
    // Strings for localization
    
    static let heightLabel = NSLocalizedString("Height", comment: "Translate text: heightLabel")
    static let powerLabel = NSLocalizedString("Power", comment: "Translate text: tabBarTitle_Discover")
    static let abilitiesLabel = NSLocalizedString("Abilities", comment: "Translate text: tabBarTitle_Who")
    static let groupsLabel = NSLocalizedString("Groups", comment: "Translate text: tabBarTitle_Listen")

}
