//
//  Constants.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import UIKit

struct API {
    
    static let BaseURL = URL(string: "https://api.myjson.com/bins/bvyob")
    
}

struct Constants {
    
    struct ConfigData {
        static let AppTitle = "Heroes World"
        static let ColorScheme = Constants.CustomColors.LightTheme.self
    }
    
    struct CustomColors {
        struct LightTheme {
            static let clearColor = UIColor.clear
            static let customTextColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            static let inverseTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            static let mediumBackgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            static let lightBackgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            static let darkBackgroundColor = #colorLiteral(red: 0.1014049426, green: 0.1172357425, blue: 0.1421842575, alpha: 1)
            static let invertedBackgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            static let navBarBackgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    struct CustomFonts {
        static let standarCustomFontName = "Avenir"
        static let boldCustomFontName = "Avenir-Black"
    }
    
    struct textSize {
        static let smallTextSize: CGFloat = 12.0
        static let mediumTextSize: CGFloat = 18.0
        static let largeTextSize: CGFloat = 22.0
        static let extrLargeTextSize: CGFloat = 34.0
    }
}
