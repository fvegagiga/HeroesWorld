//
//  DataManager.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import Foundation

enum DataManagerError: Error {
    case unknown
    case failedRequest
    case invalidResponse
}


final class DataManager {
    
    typealias HeroesDataCompletion = ([Hero]?, DataManagerError?) -> ()
    
    // MARK: - Properties
    
    private let baseURL: URL
    
    // MARK: - Initialization
    
    init(baseURL: URL) {
        self.baseURL = baseURL
    }

    
    // MARK: - Requesting Data
    public func requestHeroesData(completion: @escaping HeroesDataCompletion) {
        
        let request = URLRequest(url: baseURL)
        let task = URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let _ = error {
                completion(nil, .failedRequest)
                return
            }
            
            guard let response = urlResponse as? HTTPURLResponse,
                response.statusCode.isSuccessHTTPCode else {
                    completion(nil, .failedRequest)
                    return
            }
            
            guard let heroesData = data else {
                completion(nil, .failedRequest)
                return
            }
            
            let decoder = JSONDecoder()
            if let result = try? decoder.decode(SuperHeroes.self, from: heroesData) {
                completion(result.superheroes, nil)
            } else {
                completion(nil, .invalidResponse)
            }
        }
        task.resume()
    }
    
}
