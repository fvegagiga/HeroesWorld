//
//  ImageManager.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import Foundation
import UIKit

final class ImageCacheManager {
    
    // MARK: Cache Configuration
    
    static let imageCache: NSCache<NSString, UIImage> = {
        let cache = NSCache<NSString, UIImage>()
        
        cache.name = "ImageCacheManager"
        
        // Max 20 images in memory.
        cache.countLimit = 20
        
        // Max 10MB used.
        cache.totalCostLimit = 10 * 1024 * 1024
        
        return cache
    }()
    
    // MARK: Image Cache Methods
    
    func cachedImage(url: URL) -> UIImage? {
        return ImageCacheManager.imageCache.object(forKey: url.absoluteString as NSString)
    }
    
    func fetchImage(url: URL, completion: @escaping ((UIImage?) -> Void)) {
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil,
                let urlResponse = response as? HTTPURLResponse,
                urlResponse.statusCode.isSuccessHTTPCode,
                let data = data else {
                    completion(nil)
                    return
            }
            
            if let image = UIImage(data: data) {
                ImageCacheManager.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                completion(image)
            } else {
                completion(nil)
            }
        }
        task.resume()
    }
    
}
