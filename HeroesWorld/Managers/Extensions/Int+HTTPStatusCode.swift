//
//  Int+HTTPStatusCode.swift
//  HeroesWorld
//
//  Created by Fernando Vega Giganto on 13/3/18.
//  Copyright © 2018 Fernando Vega Giganto. All rights reserved.
//

import Foundation

extension Int {
    public var isSuccessHTTPCode: Bool {
        return 200 <= self && self < 300
    }
}

