#  HeroesWorld
## Autor: Fernando Vega Giganto

### Descripción:
Desarrollo de una aplicación para iOS que muestra información de los superhéroes del mundo Marvel.

**Arquitectura:**
Se ha empleado el patrón de diseño MVVM para desarrollar la aplicación.

**Dependencias:**
Ninguna. No se han utilizado librerías externas.
